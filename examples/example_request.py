import requests

PICTURE_SAVE_PATH = '/tmp/result.png'
REQUEST_PARAMS = {"north": 0, "east": 0, "down": -20, "heading": 5}

if __name__ == '__main__':
    r = requests.post(
        "http://localhost:5000", data=REQUEST_PARAMS
    )

    if r.status_code == 200:
        with open(PICTURE_SAVE_PATH, 'wb') as rendered_picture:
            rendered_picture.write(r.content)
        print('Done, picture saved at', PICTURE_SAVE_PATH)

    else:
        print('Something went wrong:')
        print('Status code:', r.status_code)
        print('Response content:', r.content)