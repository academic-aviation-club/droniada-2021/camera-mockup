# Installation

```bash
sudo pacman -Syu blender python-flask
```

*Note:* `flask` **has** to be installed as a system-wide package.
That's how blender's built-in ppython interpreter works

# Usage

In a spare terminal window, run

```bash
blender --background --python main.py
```

Then, use your browser or any other *http* client and connect to
```
http://localhost:5000/<X>/<Y>/<Z>/<heading>
```

for example, `http://localhost:5000/1/2/10/30` would take a picture at
X = 1, Y = 2, Z = 10 and heading = 30

