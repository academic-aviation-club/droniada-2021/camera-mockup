FROM registry.gitlab.com/academic-aviation-club/droniada-2021/droniada-docker-builder:latest

COPY main.py /

CMD xvfb-run -a bash -c "blender --background --python main.py"
