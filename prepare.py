import sys
sys.path.append('')

import bmesh
import bpy
import random
from config import *


def prepare_scene():
    bpy.data.objects.remove(bpy.context.selected_objects[0])
    bpy.data.objects['Light'].data.type= 'SUN'
    bpy.data.objects['Light'].data.energy = 1;

    light_green = bpy.data.materials.new("PKHG")
    dark_green = bpy.data.materials.new("PKHG")

    light_green.diffuse_color = (0.1, 0.8, 0.2, 1)
    dark_green.diffuse_color = (0.3, 0.8, 0.4, 1)
   
    # Create the 'grass'
    for i in range(SCENE_RANGE_MIN, SCENE_RANGE_MAX, CHUNK_SIZE):
        for j in range(SCENE_RANGE_MIN, SCENE_RANGE_MAX, CHUNK_SIZE):
            bpy.ops.mesh.primitive_plane_add(size=2, location=(i, j, 0))
            bpy.context.selected_objects[0].active_material = (
                light_green if (i+j)//CHUNK_SIZE % 2 == 0 else dark_green
            )

    bpy.ops.wm.save_as_mainfile(filepath=PREPARED_SCENE_SAVE_PATH)

if __name__ == '__main__':
    prepare_scene()
