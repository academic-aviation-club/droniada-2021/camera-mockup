# Do not run this file directly!!
# Run it from inside blender with 
# `blender --background --python main.py`
# or use the `run.sh` script in the repo

import sys
import os
sys.path.append('')

import bmesh
import bpy
from flask import Flask, send_file, request
import random
from config import *
from prepare import prepare_scene


if __name__ == "__main__":

    if not os.path.exists(PREPARED_SCENE_SAVE_PATH):
        print('No cached scene found, preparing new')
        prepare_scene()
    else:
        print('Using cached scene')

    bpy.ops.wm.open_mainfile(filepath=PREPARED_SCENE_SAVE_PATH)
    bpy.data.objects.remove(bpy.context.selected_objects[0])

    light_green = bpy.data.materials.new("PKHG")
    dark_green = bpy.data.materials.new("PKHG")

    light_green.diffuse_color = (0.1, 0.8, 0.2, 1)
    dark_green.diffuse_color = (0.3, 0.8, 0.4, 1)
   
    # Create the 'grass'
    for i in range(SCENE_RANGE_MIN, SCENE_RANGE_MAX, CHUNK_SIZE):
        for j in range(SCENE_RANGE_MIN, SCENE_RANGE_MAX, CHUNK_SIZE):
            bpy.ops.mesh.primitive_plane_add(size=2, location=(i, j, 0))
            bpy.context.selected_objects[0].active_material = (
                light_green if (i+j)//CHUNK_SIZE % 2 == 0 else dark_green
            )

    for i in range(10):
        # Create some fake markers on the ground
        X = random.randrange(-SCENE_RANGE_MAX, SCENE_RANGE_MAX)
        Y = random.randrange(-SCENE_RANGE_MAX, SCENE_RANGE_MAX)

        bpy.ops.mesh.primitive_cylinder_add(location=(X,Y,0.1), scale=(2,2,0))

    camera = bpy.context.scene.camera
    camera.rotation_mode = 'XYZ'
    camera.rotation_euler[0] = 0
    camera.rotation_euler[1] = 0

    app = Flask(__name__)

    @app.route('/', methods=["POST"])
    def hello_world():

        north = request.form['north']
        east = request.form['east']
        down = request.form['down']
        heading = request.form['heading']

        north = float(north)
        east = float(east)
        down = float(down)
        heading = float(heading)

        camera.location.x = north
        camera.location.y = east
        camera.location.z = -down
        
        camera.rotation_euler[2] = heading*(heading/180.0)
    
        bpy.context.scene.render.filepath = "/tmp/out.png"
        bpy.context.scene.render.resolution_x = 1920 // 2  
        bpy.context.scene.render.resolution_y = 1080 // 2
        bpy.ops.render.render(write_still=True)

        # print(x, y, z)
        return send_file("/tmp/out.png")

    app.run(host='0.0.0.0')
